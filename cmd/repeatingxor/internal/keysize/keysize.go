package keysize

import (
	"repeating-xor/cmd/repeatingxor/internal/chunk"
	"repeating-xor/hamming"
)

type Key struct {
	Length int
	Size float64
}

func Guess(data []byte) (keys []Key) {
	for i := 2; i <= 42; i++ {
		n, chunks := chunk.Create(data, i)
		if n == 0 {
			panic("n too small")
		}

		var distance int
		for j := 0; j < len(chunks); j++ {
			if j+1 >= len(chunks) {
				break
			}
			d, err := hamming.Distance(chunks[j], chunks[j+1])
			if err != nil {
				panic(err)
			}

			distance += d
		}

		avg := float64(i * n)
		normalized := float64(distance) / avg

		keys = append(keys, Key{ Length: i, Size: normalized })
	}
	return
}
