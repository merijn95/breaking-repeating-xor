package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"repeating-xor/cmd/repeatingxor/internal/chunk"
	"repeating-xor/cmd/repeatingxor/internal/keysize"
	"repeating-xor/letterfrequency"
	"repeating-xor/xor"
	"sort"
)

func main() {
	data, err := ioutil.ReadFile("encrypted.txt")
	if err != nil {
		panic(err)
	}

	d := string(data)
	decoded, err := base64.StdEncoding.DecodeString(d)
	if err != nil {
		panic(err)
	}

	// guess key size based on data
	keys := keysize.Guess(decoded)

	sort.Slice(keys, func(i, j int) bool {
		return keys[i].Size < keys[j].Size
	})

	// for each key
	var realKey string
	for _, key := range keys[:1] {
		var transposedBlocks [][]byte
		_, chunks := chunk.Create(decoded, key.Length)
		for i := 0; i < key.Length; i++ {
			var block []byte
			for _, chunk := range chunks {
				block = append(block, chunk[i])
			}

			transposedBlocks = append(transposedBlocks, block)
		}

		for _, transposedBlock := range transposedBlocks {
			// got one block, decrypt it
			score, err := letterfrequency.FindBestScore(transposedBlock)
			if err != nil {
				panic(err)
			}

			realKey += string(score.Character)
		}
	}

	k := xor.Key{Value: realKey }
	for _, d := range decoded {
		fmt.Print(string(d^k.Current()[0]))
		k.MovePos()
	}

	fmt.Println(realKey)
}