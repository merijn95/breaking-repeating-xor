package chunk_test

import (
	"fmt"
	"repeating-xor/cmd/repeatingxor/internal/chunk"
	"testing"
)

func TestCreate(t *testing.T) {
	str := "helloworldhelloworldhelloworldhelloworld"
	const size =  2
	_, chunks := chunk.Create([]byte(str), size)


	if string(chunks[0]) != "he" {
		t.Errorf("he != %s", string(chunks[0]))
	}
	if string(chunks[1]) != "ll" {
		t.Errorf("ll != %s", string(chunks[1]))
	}
	if string(chunks[2]) != "ow" {
		t.Errorf("ow != %s", string(chunks[2]))
	}
	if string(chunks[3]) != "or" {
		t.Errorf("or != %s", string(chunks[3]))
	}
	if string(chunks[4]) != "ld" {
		t.Errorf("ld != %s", string(chunks[4]))
	}

	for _, chunk := range chunks {
		fmt.Println(string(chunk))
	}
}