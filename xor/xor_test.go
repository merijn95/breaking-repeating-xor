package xor_test

import (
	"repeating-xor/xor"
	"strings"
	"testing"
)

func TestBreak(t *testing.T) {
	const expected = "Cooking MC's like a pound of bacon"
	const key = byte('X')

	input := []byte{27, 55, 55, 51, 49, 54, 63, 120, 21, 27, 127, 43, 120, 52, 49, 51, 61, 120, 57, 120, 40, 55, 45, 54, 60, 120, 55, 62, 120, 58, 57, 59, 55, 54}
	output := xor.Break(input, key)

	if !strings.EqualFold(expected, string(output)) {
		t.Errorf("output %s does not match expected %s", string(output), expected)
	}
}
