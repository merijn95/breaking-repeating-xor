package xor

func Break(data []byte, key byte) (result []byte) {
	for i := 0; i < len(data); i++ {
		result = append(result, data[i]^key)
	}

	return
}
