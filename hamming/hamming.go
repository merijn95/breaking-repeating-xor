package hamming

import "errors"

var ErrSliceSize = errors.New("slices are not the same size")

func Distance(s1, s2 []byte) (distance int, err error) {
	if len(s1) != len(s2) {
		return 0, ErrSliceSize
	}

	for i := 0; i < len(s1); i++ {
		for j := 0; j < 8; j++ {
			mask := byte(1 << byte(j))
			if s1[i] & mask != s2[i] & mask {
				distance++
			}
		}
	}
	return
}