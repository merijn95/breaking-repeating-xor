package chunk

func Create(data []byte, size int) (n int, chunks [][]byte) {
	chunks = append(chunks, data[:size])

	for i := size; i < len(data); i += size {
		chunk := data[i:size+i]
		chunks = append(chunks, chunk)
	}

	n = len(chunks)

	return
}