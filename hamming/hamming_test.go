package hamming_test

import (
	"repeating-xor/hamming"
	"testing"
)

func TestDistance(t *testing.T) {
	const str1, str2 = "wokka wokka!!!", "this is a test"
	const expectedDistance = 37

	distance, err := hamming.Distance([]byte(str1), []byte(str2))
	// sanity check
	if err != nil {
		t.Fatal(err)
	}

	if distance != expectedDistance {
		t.Errorf("expected distance to be %d, got %d", expectedDistance, distance)
	}
}

func TestDistance_MustBeSameLength(t *testing.T) {
	const str1, str2 = "aa", "b"

	_, err := hamming.Distance([]byte(str1), []byte(str2))
	if err != hamming.ErrSliceSize {
		t.Errorf("expected ErrSizeSlice, got %s instead", err)
	}
}