package letterfrequency

type Score struct {
	Character byte
	Result    string
	Score     float64
}